export default {
    url: 'http://localhost:8090/', // URL de votre serveur Keycloak
    realm: 'SpringBootKeycloak', // Le nom de votre realm Keycloak
    clientId: 'frontend-app', // L'ID du client Keycloak que vous avez configuré
  };