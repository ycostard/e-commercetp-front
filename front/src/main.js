/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'

// Plugins
import { registerPlugins } from '@/plugins'
import Keycloak from 'keycloak-js'; // Importez Keycloak ici
import keycloakConfig from '../keycloak-config'; // Assurez-vous d'importer votre configuration Keycloak

const app = createApp(App)

registerPlugins(app)

// Créez une nouvelle instance de Keycloak avec 'new'
const keycloak = new Keycloak(keycloakConfig);

// Initialisation de Keycloak
keycloak.init({ onLoad: 'check-sso' }).then((authenticated) => {
  // Montez votre application Vue.js après l'initialisation de Keycloak
  app.mount('#app');
});

export default keycloak; 
